package com.sofka.pacientecovid.domain.repository;

import com.sofka.pacientecovid.domain.Patient;

import java.util.List;
import java.util.Optional;

public interface PatientRepository {

    List<Patient> getAll();

    Optional<Patient> getById(int patientId);

    Patient save(Patient patient);

    void delete(int patientId);
}
