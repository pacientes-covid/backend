package com.sofka.pacientecovid.domain.repository;

import com.sofka.pacientecovid.domain.Appointment;

import java.util.List;
import java.util.Optional;

public interface AppointmentRepository {

    List<Appointment> getAll();

    Optional<List<Appointment>> findByPatientId(int patientId);

    Appointment save(Appointment appointment);

}
