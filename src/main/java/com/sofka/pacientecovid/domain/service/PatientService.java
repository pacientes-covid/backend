package com.sofka.pacientecovid.domain.service;

import com.sofka.pacientecovid.domain.Patient;
import com.sofka.pacientecovid.domain.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    @Autowired
    private PatientRepository pacienteRepository;

    public List<Patient> getAll() {
        return pacienteRepository.getAll();
    }

    public Optional<Patient> getPatient(int patientId) {
        return pacienteRepository.getById(patientId);
    }

    public Patient save(Patient patient) {
        return pacienteRepository.save(patient);
    }

    public boolean delete(int patientId) {
        return getPatient(patientId).map(patient -> {
            pacienteRepository.delete(patientId);
            return true;
        }).orElse(false);
    }

}
