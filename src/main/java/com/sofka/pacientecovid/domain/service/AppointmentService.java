package com.sofka.pacientecovid.domain.service;

import com.sofka.pacientecovid.domain.Appointment;
import com.sofka.pacientecovid.domain.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepository;

    public List<Appointment> getAll() {
        return appointmentRepository.getAll();
    }

    public Optional<List<Appointment>> getByPatient(int pacienteId) {
        return appointmentRepository.findByPatientId(pacienteId);
    }

    public Appointment save(Appointment appointment) {
        return appointmentRepository.save(appointment);
    }

}
