package com.sofka.pacientecovid.domain;

public class Appointment {

    private int patientId;
    private String date;
    private String time;
    private String recommendations;
    private boolean conclude;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public boolean isConclude() {
        return conclude;
    }

    public void setConclude(boolean conclude) {
        this.conclude = conclude;
    }
}
