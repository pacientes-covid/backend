package com.sofka.pacientecovid.persistence;

import com.sofka.pacientecovid.domain.Appointment;
import com.sofka.pacientecovid.domain.repository.AppointmentRepository;
import com.sofka.pacientecovid.persistence.crud.CitaCrudRepository;
import com.sofka.pacientecovid.persistence.entity.Cita;
import com.sofka.pacientecovid.persistence.mapper.AppointmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CitaRepository implements AppointmentRepository {

    @Autowired
    private CitaCrudRepository citaCrudRepository;

    @Autowired
    private AppointmentMapper mapper;

    @Override
    public List<Appointment> getAll() {
        return mapper.toAppointments((List<Cita>) citaCrudRepository.findAll());
    }

    @Override
    public Optional<List<Appointment>> findByPatientId(int patientId) {
        return citaCrudRepository.findByIdPaciente(patientId)
                .map(citas -> mapper.toAppointments(citas));
    }
    
    @Override
    public Appointment save(Appointment appointment) {
        Cita cita = mapper.toCita(appointment);
        return mapper.toAppointment(citaCrudRepository.save(cita));
    }
}
