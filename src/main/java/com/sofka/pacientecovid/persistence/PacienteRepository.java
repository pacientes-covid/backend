package com.sofka.pacientecovid.persistence;

import com.sofka.pacientecovid.domain.Patient;
import com.sofka.pacientecovid.domain.repository.PatientRepository;
import com.sofka.pacientecovid.persistence.crud.PacienteCrudRepository;
import com.sofka.pacientecovid.persistence.entity.Paciente;
import com.sofka.pacientecovid.persistence.mapper.PatientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PacienteRepository implements PatientRepository {

    @Autowired
    private PacienteCrudRepository pacienteCrudRepository;

    @Autowired
    private PatientMapper mapper;

    @Override
    public List<Patient> getAll() {
        return mapper.toPatients((List<Paciente>) pacienteCrudRepository.findAll());
    }

    @Override
    public Optional<Patient> getById(int patientId) {
        return pacienteCrudRepository.findById(patientId)
                .map(paciente -> mapper.toPatient(paciente));
    }

    @Override
    public Patient save(Patient patient) {
        Paciente paciente = mapper.toPaciente(patient);
        return mapper.toPatient(pacienteCrudRepository.save(paciente));
    }

    @Override
    public void delete(int patientId) {
        pacienteCrudRepository.deleteById(patientId);
    }
}
