package com.sofka.pacientecovid.persistence.mapper;

import com.sofka.pacientecovid.domain.Appointment;
import com.sofka.pacientecovid.persistence.entity.Cita;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AppointmentMapper {

    @Mappings({
            @Mapping(source = "idPaciente", target = "patientId"),
            @Mapping(source = "fecha", target = "date"),
            @Mapping(source = "hora", target = "time"),
            @Mapping(source = "recomendaciones", target = "recommendations"),
            @Mapping(source = "estado", target = "conclude")
    })
    Appointment toAppointment(Cita cita);

    List<Appointment> toAppointments(List<Cita> citas);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "paciente", ignore = true),
            @Mapping(target = "idCita", ignore = true)
    })
    Cita toCita(Appointment appointment);
}
