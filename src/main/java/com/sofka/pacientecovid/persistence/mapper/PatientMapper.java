package com.sofka.pacientecovid.persistence.mapper;

import com.sofka.pacientecovid.domain.Patient;
import com.sofka.pacientecovid.persistence.entity.Paciente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatientMapper {

    @Mappings({
            @Mapping(source = "id", target = "patientId"),
            @Mapping(source = "nombres", target = "firstName"),
            @Mapping(source = "apellidos", target = "lastName"),
            @Mapping(source = "celular", target = "cellPhoneNumber"),
            @Mapping(source = "direccion", target = "address"),
            @Mapping(source = "correoElectronico", target = "email")
    })
    Patient toPatient(Paciente paciente);

    List<Patient> toPatients(List<Paciente> pacientes);

    @InheritInverseConfiguration
    @Mapping(target = "citas", ignore = true)
    Paciente toPaciente(Patient patient);
}
