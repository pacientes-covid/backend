package com.sofka.pacientecovid.persistence.crud;

import com.sofka.pacientecovid.persistence.entity.Cita;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CitaCrudRepository extends CrudRepository<Cita, Integer> {

    Optional<List<Cita>> findByIdPaciente(int idPaciente);
}
