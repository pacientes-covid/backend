package com.sofka.pacientecovid.persistence.crud;

import com.sofka.pacientecovid.persistence.entity.Paciente;
import org.springframework.data.repository.CrudRepository;

public interface PacienteCrudRepository extends CrudRepository<Paciente, Integer> {
}