package com.sofka.pacientecovid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacienteCovidApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacienteCovidApplication.class, args);
	}

}
