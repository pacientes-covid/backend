package com.sofka.pacientecovid.web.controller;


import com.sofka.pacientecovid.domain.Appointment;
import com.sofka.pacientecovid.domain.service.AppointmentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cita")
public class CitaController {

    @Autowired
    private AppointmentService appointmentService;


    @GetMapping("/all")
    @ApiOperation("Consultar todas las citas registradas en la aplicación")
    @ApiResponse(code = 200, message = "Respuesta exitosa")
    public ResponseEntity<List<Appointment>> getAll() {
        return new ResponseEntity<>(appointmentService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/paciente/{id}")
    @ApiOperation("Consultar todas las citas de un paciente")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Respuesta exitosa"),
            @ApiResponse(code = 404, message = "No se encontraron citas")
    })
    public ResponseEntity<List<Appointment>> getByPatient(@ApiParam(value = "Número de documento del paciente", required = true)
                                                          @PathVariable("id") int patientId) {
        return appointmentService.getByPatient(patientId)
                .map(appointments -> new ResponseEntity<>(appointments, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @ApiOperation("Crear una nueva cita para un paciente covid")
    @ApiResponse(code = 201, message = "Cita creada exitosamente")
    public ResponseEntity<Appointment> save(@ApiParam(value = "Datos básicos de la cita", required = true)
                                            @RequestBody Appointment appointment) {
        return new ResponseEntity<>(appointmentService.save(appointment), HttpStatus.CREATED);
    }

}
