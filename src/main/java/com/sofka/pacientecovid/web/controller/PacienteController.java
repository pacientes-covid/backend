package com.sofka.pacientecovid.web.controller;

import com.sofka.pacientecovid.domain.Patient;
import com.sofka.pacientecovid.domain.service.PatientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    @Autowired
    private PatientService patientService;

    @GetMapping("/all")
    @ApiOperation("Consultar todos los pacientes covid registrados en la aplicación")
    @ApiResponse(code = 200, message = "Respuesta exitosa")
    public ResponseEntity<List<Patient>> getAll() {
        return new ResponseEntity<>(patientService.getAll(), HttpStatus.OK);
    }

    @ApiOperation("Consultar paciente covid por número de documento")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Respuesta exitosa"),
            @ApiResponse(code = 404, message = "Paciente no encontrado")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Patient> getPaciente(@ApiParam(value = "Número de documento del paciente", required = true)
                                               @PathVariable("id") int patientId) {
        return patientService.getPatient(patientId)
                .map(paciente -> new ResponseEntity<>(paciente, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @ApiOperation("Crear un nuevo paciente covid")
    @ApiResponse(code = 201, message = "Paciente creado exitosamente")
    public ResponseEntity<Patient> save(@ApiParam(value = "Datos básicos del paciente", required = true)
                                            @RequestBody Patient patient) {
        return new ResponseEntity<>(patientService.save(patient), HttpStatus.CREATED);
    }

    @ApiOperation("Eliminar paciente covid por número de documento")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Eliminación exiosa"),
            @ApiResponse(code = 404, message = "Paciente no existente")
    })
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@ApiParam(value = "Número de documento del paciente", required = true)
                                 @PathVariable("id") int patientId) {
        if (patientService.delete(patientId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
