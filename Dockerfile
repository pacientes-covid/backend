FROM openjdk:8
COPY /build/libs/paciente-covid-*.jar /Paciente-Covid.jar
EXPOSE 8686
ENTRYPOINT ["java","-jar","/Paciente-Covid.jar"]
